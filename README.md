<a href="https://flathub.org/apps/details/com.belmoussaoui.Decoder">
<img src="https://flathub.org/assets/badges/flathub-badge-i-en.png" width="190px" />
</a>

# Decoder

<img src="https://gitlab.gnome.org/bilelmoussaoui/decoder/raw/master/data/icons/com.belmoussaoui.Decoder.svg" width="128px" height="128px" />
<p>Scan and Generate QR Codes</p>

## Screenshots

![screenshot](data/screenshots/screenshot1.png)
![screenshot](data/screenshots/screenshot2.png)
![screenshot](data/screenshots/screenshot3.png)

## Known issue

- If the application crashes once you try to use the camera scanning feature while running the application under Wayland with an Intel GPU: The issue is caused by the driver and can be worked around by forcing the application to run under X11.

You can override the permissions if you're using Flatpak with
```
flatpak override com.belmoussaoui.Decoder --nosocket=fallback-x11 --nosocket=wayland --socket=x11
```

## Getting in touch

If you have any questions regarding the use or development of Decoder, please join us on our [#authenticator:gnome.org](https://matrix.to/#/#authenticator:gnome.org) channel (it's a shared channel with Authenticator project).

## Hack on Decoder

To build the development version of Decoder and hack on the code
see the [general guide](https://wiki.gnome.org/Newcomers/BuildProject)
for building GNOME apps with Flatpak and GNOME Builder.

