use crate::qrcode::QRCode;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, prelude::*};

mod imp {
    use super::*;
    use glib::subclass;
    use std::cell::RefCell;

    #[derive(Debug)]
    pub struct QRCodeModel(pub RefCell<Vec<QRCode>>);

    impl ObjectSubclass for QRCodeModel {
        const NAME: &'static str = "QRCodeModel";
        type Type = super::QRCodeModel;
        type ParentType = glib::Object;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn type_init(type_: &mut subclass::InitializingType<Self>) {
            type_.add_interface::<gio::ListModel>();
        }

        fn new() -> Self {
            Self(RefCell::new(Vec::new()))
        }
    }
    impl ObjectImpl for QRCodeModel {}
    impl ListModelImpl for QRCodeModel {
        fn get_item_type(&self, _list_model: &Self::Type) -> glib::Type {
            QRCode::static_type()
        }
        fn get_n_items(&self, _list_model: &Self::Type) -> u32 {
            self.0.borrow().len() as u32
        }
        fn get_item(&self, _list_model: &Self::Type, position: u32) -> Option<glib::Object> {
            self.0
                .borrow()
                .get(position as usize)
                .map(|o| o.clone().upcast::<glib::Object>())
        }
    }
}

glib::wrapper! {
    pub struct QRCodeModel(ObjectSubclass<imp::QRCodeModel>) @implements gio::ListModel;
}

impl QRCodeModel {
    #[allow(clippy::new_without_default)]
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create QRCodeModel")
    }

    pub fn position(&self, code_a: &QRCode) -> Option<u32> {
        for pos in 0..self.get_n_items() {
            let obj = self.get_object(pos)?;
            let code = obj.downcast::<QRCode>().unwrap();
            if code.id() == code_a.id() {
                return Some(pos);
            }
        }
        None
    }

    pub fn append(&self, code: &QRCode) {
        let self_ = imp::QRCodeModel::from_instance(self);
        self_.0.borrow_mut().insert(0, code.clone());
        self.items_changed(0, 0, 1);
    }

    pub fn remove(&self, pos: u32) {
        let self_ = imp::QRCodeModel::from_instance(self);
        self_.0.borrow_mut().remove(pos as usize);
        self.items_changed(pos, 1, 0);
    }

    pub fn init(&self) {
        // fill in the codes from the database
        QRCode::load()
            .expect("Failed to load codes from the database")
            .for_each(|code| {
                self.append(&code);
            });
    }
}
