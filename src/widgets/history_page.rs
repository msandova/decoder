use crate::config;
use crate::model::QRCodeModel;
use crate::qrcode::QRCode;
use crate::widgets::QRCodeRow;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct HistoryPage {
        #[template_child]
        pub stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub listbox: TemplateChild<gtk::ListBox>,
        #[template_child]
        pub empty_page: TemplateChild<libhandy::StatusPage>,
    }

    impl ObjectSubclass for HistoryPage {
        const NAME: &'static str = "HistoryPage";
        type Type = super::HistoryPage;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/history_page.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                stack: TemplateChild::default(),
                listbox: TemplateChild::default(),
                empty_page: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for HistoryPage {}
    impl WidgetImpl for HistoryPage {}
    impl BoxImpl for HistoryPage {}
}

glib::wrapper! {
    pub struct HistoryPage(ObjectSubclass<imp::HistoryPage>) @extends gtk::Widget, gtk::Box;
}

impl HistoryPage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create a HistoryPage widget")
    }

    pub fn set_model(&self, model: &QRCodeModel) {
        let self_ = imp::HistoryPage::from_instance(self);

        self_.empty_page.get().set_icon_name(Some(&config::APP_ID));

        let stack = self_.stack.get();
        stack.set_visible_child_name("empty");
        model.connect_items_changed(glib::clone!(@weak model, @weak stack => move |_, _, _, _| {
            if model.get_n_items() == 0 {
                stack.set_visible_child_name("empty");
            } else {
                stack.set_visible_child_name("content");
            }
        }));

        self_.listbox.get().bind_model(Some(model), glib::clone!(@weak model => move |item| {
            let code = item.clone().downcast::<QRCode>().unwrap();

            let row = QRCodeRow::new(code.clone());
            row.connect_local("deleted", false, glib::clone!(@weak model, @weak code => move |_| {
                if let Some(pos) = model.position(&code) {
                    model.remove(pos);
                }
                None
            })).unwrap();
            row.upcast()
        }));
    }
}
