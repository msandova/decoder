use crate::{
    application::Application,
    config,
    model::QRCodeModel,
    qrcode::QRCode,
    widgets::{Camera, HistoryPage, QRCodeCreatePage, QRCodePage},
};
use gtk::subclass::prelude::*;
use gtk::{gio, glib, prelude::*, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;
    use libhandy::subclass::application_window::ApplicationWindowImpl as HdyApplicationWindowImpl;

    #[derive(Debug, CompositeTemplate)]
    pub struct Window {
        pub model: QRCodeModel,
        #[template_child]
        pub camera: TemplateChild<Camera>,
        #[template_child]
        pub deck: TemplateChild<libhandy::Leaflet>,
        #[template_child]
        pub main_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub create_page: TemplateChild<QRCodeCreatePage>,
        #[template_child]
        pub code_page: TemplateChild<QRCodePage>,
        #[template_child]
        pub history_page: TemplateChild<HistoryPage>,
        #[template_child]
        pub dark_mode_button: TemplateChild<gtk::Button>,
        #[template_child]
        pub view_switcher_title: TemplateChild<libhandy::ViewSwitcherTitle>,
        #[template_child]
        pub switcher_bar: TemplateChild<libhandy::ViewSwitcherBar>,
    }

    impl ObjectSubclass for Window {
        const NAME: &'static str = "Window";
        type Type = super::Window;
        type ParentType = libhandy::ApplicationWindow;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn new() -> Self {
            Self {
                model: QRCodeModel::new(),
                camera: TemplateChild::default(),
                deck: TemplateChild::default(),
                main_stack: TemplateChild::default(),
                history_page: TemplateChild::default(),
                create_page: TemplateChild::default(),
                code_page: TemplateChild::default(),
                dark_mode_button: TemplateChild::default(),
                switcher_bar: TemplateChild::default(),
                view_switcher_title: TemplateChild::default(),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            QRCodeCreatePage::static_type();
            QRCodePage::static_type();
            Camera::static_type();
            HistoryPage::static_type();
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/window.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for Window {}
    impl WidgetImpl for Window {}
    impl WindowImpl for Window {}
    impl ApplicationWindowImpl for Window {}
    impl HdyApplicationWindowImpl for Window {}
}

glib::wrapper! {
    pub struct Window(ObjectSubclass<imp::Window>)
        @extends gtk::Widget, gtk::Window, gtk::ApplicationWindow, libhandy::ApplicationWindow, gio::ActionMap, gio::ActionGroup;
}

impl Window {
    pub fn new(app: &Application) -> Self {
        let window = glib::Object::new::<Window>(&[("application", app)]).unwrap();
        app.add_window(&window);

        if config::PROFILE == "Devel" {
            window.get_style_context().add_class("devel");
        }
        window.init();
        window.setup_actions(app);
        window.setup_signals(app);
        window
    }

    pub fn show_code_detected(&self, code_content: &str) {
        let self_ = imp::Window::from_instance(self);
        self_.code_page.get().set_scanned_code(code_content);
        self_.deck.get().set_visible_child_name("code");
    }

    fn init(&self) {
        let self_ = imp::Window::from_instance(self);

        self_
            .camera
            .get()
            .connect_local(
                "code-detected",
                false,
                glib::clone!(@weak self as win => move |args| {
                    let code = args.get(1).unwrap().get::<String>().unwrap().unwrap();
                    win.show_code_detected(&code);

                    None
                }),
            )
            .unwrap();

        self_.history_page.get().set_model(&self_.model);
        self_.model.init();

        let deck = self_.deck.get();
        let main_stack = self_.main_stack.get();
        self_
            .code_page
            .get()
            .connect_local(
                "created",
                false,
                glib::clone!(@weak self_.model as model, @weak deck => move |args| {
                    let code = args.get(1).unwrap().get::<QRCode>().unwrap().unwrap();
                    model.append(&code);
                    deck.set_visible_child_name("main");
                    main_stack.set_visible_child_name("history");
                    None
                }),
            )
            .unwrap();

        self_
            .create_page
            .get()
            .connect_local(
                "created",
                false,
                glib::clone!(@weak self_.model as model => move |args| {
                    let code = args.get(1).unwrap().get::<QRCode>().unwrap().unwrap();
                    model.append(&code);
                    None
                }),
            )
            .unwrap();

        let gtk_settings = gtk::Settings::get_default().unwrap();
        let dark_mode_button = self_.dark_mode_button.get();
        gtk_settings.connect_property_gtk_application_prefer_dark_theme_notify(move |settings| {
            if !settings.get_property_gtk_application_prefer_dark_theme() {
                dark_mode_button.set_icon_name("dark-mode-symbolic");
            } else {
                dark_mode_button.set_icon_name("light-mode-symbolic");
            }
        });

        let switcher_bar = self_.switcher_bar.get();
        self_
            .view_switcher_title
            .get()
            .connect_property_title_visible_notify(move |view_switcher| {
                switcher_bar.set_reveal(view_switcher.get_title_visible());
            });
    }

    fn setup_actions(&self, _app: &Application) {
        let self_ = imp::Window::from_instance(self);

        let deck = self_.deck.get();
        gtk_macros::action!(
            self,
            "back",
            glib::clone!(@weak deck => move |_, _| {
                deck.set_visible_child_name("main");
            })
        );
    }

    fn setup_signals(&self, _app: &Application) {
        let self_ = imp::Window::from_instance(self);

        let camera = self_.camera.get();
        self_
            .main_stack
            .get()
            .connect_property_visible_child_name_notify(
                glib::clone!(@weak camera => move |stack| {
                   match stack.get_visible_child_name().unwrap().as_ref() {
                       "scan" => camera.start(),
                       _ => camera.stop(),
                   };
                }),
            );
    }
}
