use super::QRCodePaintable;
use crate::qrcode::{QRCode, QRCodeData};
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};
use gtk_macros::action;

mod imp {
    use super::*;
    use glib::subclass;
    use std::cell::RefCell;

    #[derive(CompositeTemplate)]
    pub struct QRCodePage {
        #[template_child]
        pub picture: TemplateChild<gtk::Picture>,
        pub paintable: QRCodePaintable,
        pub actions: gio::SimpleActionGroup,
        pub scanned_code: RefCell<Option<String>>,
    }

    impl ObjectSubclass for QRCodePage {
        const NAME: &'static str = "QRCodePage";
        type Type = super::QRCodePage;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_page.ui");
            Self::bind_template_children(klass);
            klass.add_signal(
                "created",
                glib::SignalFlags::RUN_FIRST,
                &[QRCode::static_type()],
                glib::Type::Unit,
            );
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                actions: gio::SimpleActionGroup::new(),
                picture: TemplateChild::default(),
                paintable: QRCodePaintable::new(),
                scanned_code: RefCell::new(None),
            }
        }
    }

    impl ObjectImpl for QRCodePage {
        fn constructed(&self, obj: &Self::Type) {
            obj.init_widgets();
            obj.init_actions();
            self.parent_constructed(obj);
        }
    }

    impl WidgetImpl for QRCodePage {}
    impl BoxImpl for QRCodePage {}
}

glib::wrapper! {
    pub struct QRCodePage(ObjectSubclass<imp::QRCodePage>) @extends gtk::Widget, gtk::Box;
}

impl QRCodePage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create a QRCodePage widget")
    }

    fn init_widgets(&self) {
        let self_ = imp::QRCodePage::from_instance(self);
        self_.picture.get().set_paintable(Some(&self_.paintable));
    }

    fn save(&self) {
        let self_ = imp::QRCodePage::from_instance(self);
        if let Some(code_content) = self_.scanned_code.borrow_mut().take() {
            let qr_code = QRCode::create(&code_content).unwrap();
            self.emit("created", &[&qr_code]).unwrap();
        }
    }

    fn init_actions(&self) {
        let self_ = imp::QRCodePage::from_instance(self);

        action!(
            self_.actions,
            "save",
            glib::clone!(@weak self as page => move |_, _| {
                page.save();
            })
        );
        self.insert_action_group("page", Some(&self_.actions));
    }

    pub fn set_qrcode(&self, code: QRCodeData) {
        let self_ = imp::QRCodePage::from_instance(self);
        self_.paintable.set_qrcode(code);
    }

    // Specific mode when the code was scanned and not a saved one
    pub fn set_scanned_code(&self, code: &str) {
        let self_ = imp::QRCodePage::from_instance(self);
        let data = QRCodeData::from(code);
        self.set_qrcode(data);
        self_.scanned_code.replace(Some(code.to_string()));
    }
}
