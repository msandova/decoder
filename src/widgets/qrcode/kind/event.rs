use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeEvent {
        #[template_child]
        pub summary_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub start_at_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub end_at_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub location_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub description_label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeEvent {
        const NAME: &'static str = "QRCodeEvent";
        type Type = super::QRCodeEvent;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_event.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                summary_label: TemplateChild::default(),
                start_at_label: TemplateChild::default(),
                end_at_label: TemplateChild::default(),
                location_label: TemplateChild::default(),
                description_label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeEvent {}
    impl WidgetImpl for QRCodeEvent {}
    impl BoxImpl for QRCodeEvent {}
}

glib::wrapper! {
    pub struct QRCodeEvent(ObjectSubclass<imp::QRCodeEvent>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeEvent {
    pub fn new(
        summary: String,
        start_at: String,
        end_at: String,
        description: Option<String>,
        location: Option<String>,
    ) -> Self {
        let widget =
            glib::Object::new::<QRCodeEvent>(&[]).expect("Failed to create a QRCodeEvent widget");
        widget.init(summary, start_at, end_at, description, location);
        widget
    }

    fn init(
        &self,
        summary: String,
        start_at: String,
        end_at: String,
        description: Option<String>,
        location: Option<String>,
    ) {
        let self_ = imp::QRCodeEvent::from_instance(self);
        self_.summary_label.get().set_label(&summary);
        self_.start_at_label.get().set_label(&start_at);
        self_.end_at_label.get().set_label(&end_at);

        if let Some(ref description) = description {
            self_.description_label.get().set_label(&description);
        }
        if let Some(ref location) = location {
            self_.location_label.get().set_label(&location);
        }
    }
}
