use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeSMS {
        #[template_child]
        pub phone_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub content_label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeSMS {
        const NAME: &'static str = "QRCodeSMS";
        type Type = super::QRCodeSMS;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_sms.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                phone_label: TemplateChild::default(),
                content_label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeSMS {}
    impl WidgetImpl for QRCodeSMS {}
    impl BoxImpl for QRCodeSMS {}
}

glib::wrapper! {
    pub struct QRCodeSMS(ObjectSubclass<imp::QRCodeSMS>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeSMS {
    pub fn new(phone: String, content: String) -> Self {
        let widget =
            glib::Object::new::<QRCodeSMS>(&[]).expect("Failed to create a QRCodeSMS widget");
        widget.init(phone, content);
        widget
    }

    fn init(&self, phone: String, content: String) {
        let self_ = imp::QRCodeSMS::from_instance(self);

        self_.phone_label.get().set_label(&phone);
        self_.content_label.get().set_label(&content);
    }
}
