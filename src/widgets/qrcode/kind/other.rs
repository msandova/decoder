use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeOther {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeOther {
        const NAME: &'static str = "QRCodeOther";
        type Type = super::QRCodeOther;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_other.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeOther {}
    impl WidgetImpl for QRCodeOther {}
    impl BoxImpl for QRCodeOther {}
}

glib::wrapper! {
    pub struct QRCodeOther(ObjectSubclass<imp::QRCodeOther>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeOther {
    pub fn new(content: String) -> Self {
        let widget =
            glib::Object::new::<QRCodeOther>(&[]).expect("Failed to create a QRCodeOther widget");
        widget.init(content);
        widget
    }

    fn init(&self, content: String) {
        let self_ = imp::QRCodeOther::from_instance(self);

        self_.label.get().set_label(&content);
    }
}
