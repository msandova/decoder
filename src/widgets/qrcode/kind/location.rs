use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeLocation {
        #[template_child]
        pub longitude_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub latitude_label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeLocation {
        const NAME: &'static str = "QRCodeLocation";
        type Type = super::QRCodeLocation;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass
                .set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_location.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                longitude_label: TemplateChild::default(),
                latitude_label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeLocation {}
    impl WidgetImpl for QRCodeLocation {}
    impl BoxImpl for QRCodeLocation {}
}

glib::wrapper! {
    pub struct QRCodeLocation(ObjectSubclass<imp::QRCodeLocation>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeLocation {
    pub fn new(latitude: String, longitude: String) -> Self {
        let widget = glib::Object::new::<QRCodeLocation>(&[])
            .expect("Failed to create a QRCodeLocation widget");
        widget.init(latitude, longitude);
        widget
    }

    fn init(&self, latitude: String, longitude: String) {
        let self_ = imp::QRCodeLocation::from_instance(self);
        self_.longitude_label.get().set_label(&longitude);
        self_.latitude_label.get().set_label(&latitude);
    }
}
