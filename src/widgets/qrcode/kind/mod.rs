mod event;
mod location;
mod mail;
mod other;
mod sms;
mod url;
mod wifi;

pub use self::url::QRCodeUrl;
pub use event::QRCodeEvent;
pub use location::QRCodeLocation;
pub use mail::QRCodeMail;
pub use other::QRCodeOther;
pub use sms::QRCodeSMS;
pub use wifi::QRCodeWifi;
