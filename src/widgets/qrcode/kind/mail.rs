use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeMail {
        #[template_child]
        pub to_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub subject_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub body_label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeMail {
        const NAME: &'static str = "QRCodeMail";
        type Type = super::QRCodeMail;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_mail.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                to_label: TemplateChild::default(),
                subject_label: TemplateChild::default(),
                body_label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeMail {}
    impl WidgetImpl for QRCodeMail {}
    impl BoxImpl for QRCodeMail {}
}

glib::wrapper! {
    pub struct QRCodeMail(ObjectSubclass<imp::QRCodeMail>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeMail {
    pub fn new(to: String, subject: Option<String>, body: Option<String>) -> Self {
        let widget =
            glib::Object::new::<QRCodeMail>(&[]).expect("Failed to create a QRCodeMail widget");
        widget.init(to, subject, body);
        widget
    }

    fn init(&self, to: String, subject: Option<String>, body: Option<String>) {
        let self_ = imp::QRCodeMail::from_instance(self);
        self_.to_label.get().set_label(&to);
        if let Some(ref subject) = subject {
            self_.subject_label.get().set_label(&subject);
        }
        if let Some(ref body) = body {
            self_.body_label.get().set_label(&body);
        }
    }
}
