use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeWifi {
        #[template_child]
        pub network_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub password_label: TemplateChild<gtk::Label>,
        #[template_child]
        pub encryption_label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeWifi {
        const NAME: &'static str = "QRCodeWifi";
        type Type = super::QRCodeWifi;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_wifi.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                network_label: TemplateChild::default(),
                password_label: TemplateChild::default(),
                encryption_label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeWifi {}
    impl WidgetImpl for QRCodeWifi {}
    impl BoxImpl for QRCodeWifi {}
}

glib::wrapper! {
    pub struct QRCodeWifi(ObjectSubclass<imp::QRCodeWifi>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeWifi {
    pub fn new(network: String, password: String, encryption: String) -> Self {
        let widget =
            glib::Object::new::<QRCodeWifi>(&[]).expect("Failed to create a QRCodeWifi widget");
        widget.init(network, password, encryption);
        widget
    }

    fn init(&self, network: String, password: String, encryption: String) {
        let self_ = imp::QRCodeWifi::from_instance(self);

        self_.network_label.get().set_label(&network);
        self_.password_label.get().set_label(&password);
        self_.encryption_label.get().set_label(&encryption);
    }
}
