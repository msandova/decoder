use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{glib, CompositeTemplate};

mod imp {
    use super::*;
    use glib::subclass;

    #[derive(CompositeTemplate)]
    pub struct QRCodeUrl {
        #[template_child]
        pub label: TemplateChild<gtk::Label>,
    }

    impl ObjectSubclass for QRCodeUrl {
        const NAME: &'static str = "QRCodeUrl";
        type Type = super::QRCodeUrl;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_kind_url.ui");
            Self::bind_template_children(klass);
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            Self {
                label: TemplateChild::default(),
            }
        }
    }
    impl ObjectImpl for QRCodeUrl {}
    impl WidgetImpl for QRCodeUrl {}
    impl BoxImpl for QRCodeUrl {}
}

glib::wrapper! {
    pub struct QRCodeUrl(ObjectSubclass<imp::QRCodeUrl>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeUrl {
    pub fn new(url: url::Url) -> Self {
        let widget =
            glib::Object::new::<QRCodeUrl>(&[]).expect("Failed to create a QRCodeUrl widget");
        widget.init(url);
        widget
    }

    fn init(&self, url: url::Url) {
        let self_ = imp::QRCodeUrl::from_instance(self);

        self_
            .label
            .get()
            .set_markup(&format!("<a href='{}'>{}</a>", url, url));
    }
}
