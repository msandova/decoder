use crate::qrcode::QRCode;
use crate::widgets::QRCodePaintable;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, prelude::*, CompositeTemplate};
use gtk_macros::action;
use std::cell::RefCell;

mod imp {
    use super::*;
    use glib::subclass;

    static PROPERTIES: [subclass::Property; 1] = [subclass::Property("code", |name| {
        glib::ParamSpec::object(
            name,
            "QRCode",
            "The associated QRCode",
            QRCode::static_type(),
            glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
        )
    })];

    #[derive(CompositeTemplate)]
    pub struct QRCodeRow {
        pub actions: gio::SimpleActionGroup,
        pub code: RefCell<Option<QRCode>>,
        #[template_child]
        pub picture: TemplateChild<gtk::Picture>,
        #[template_child]
        pub revealer: TemplateChild<gtk::Revealer>,
        #[template_child]
        pub container: TemplateChild<gtk::Box>,
        #[template_child]
        pub created_at_label: TemplateChild<gtk::Label>,
        pub paintable: QRCodePaintable,
    }

    impl ObjectSubclass for QRCodeRow {
        const NAME: &'static str = "QRCodeRow";
        type Type = super::QRCodeRow;
        type ParentType = gtk::ListBoxRow;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn new() -> Self {
            Self {
                actions: gio::SimpleActionGroup::new(),
                code: RefCell::new(None),
                paintable: QRCodePaintable::new(),
                container: TemplateChild::default(),
                picture: TemplateChild::default(),
                revealer: TemplateChild::default(),
                created_at_label: TemplateChild::default(),
            }
        }

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_row.ui");
            klass.add_signal(
                "deleted",
                glib::SignalFlags::RUN_LAST,
                &[],
                glib::Type::Unit,
            );
            Self::bind_template_children(klass);
            klass.install_properties(&PROPERTIES);
        }
        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }
    }

    impl ObjectImpl for QRCodeRow {
        fn set_property(&self, _obj: &Self::Type, id: usize, value: &glib::Value) {
            let prop = &PROPERTIES[id];

            match *prop {
                subclass::Property("code", ..) => {
                    let code = value.get().unwrap();
                    self.code.replace(code);
                }
                _ => unimplemented!(),
            }
        }

        fn get_property(&self, _obj: &Self::Type, id: usize) -> glib::Value {
            let prop = &PROPERTIES[id];
            match *prop {
                subclass::Property("code", ..) => self.code.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
    impl WidgetImpl for QRCodeRow {}
    impl ListBoxRowImpl for QRCodeRow {
        fn activate(&self, _row: &Self::Type) {
            let revealer = self.revealer.get();
            revealer.set_reveal_child(!revealer.get_reveal_child());
        }
    }
}

glib::wrapper! {
    pub struct QRCodeRow(ObjectSubclass<imp::QRCodeRow>) @extends gtk::Widget, gtk::ListBoxRow;
}

impl QRCodeRow {
    pub fn new(code: QRCode) -> Self {
        let row: QRCodeRow =
            glib::Object::new(&[("code", &code)]).expect("Failed to create QRCodeRow");
        row.init_widgets();
        row.init_actions();
        row
    }

    fn code(&self) -> QRCode {
        let code = self.get_property("code").unwrap();
        code.get::<QRCode>().unwrap().unwrap()
    }

    fn delete(&self) {
        let code = self.code();
        code.delete();
        self.emit("deleted", &[]).unwrap();
    }

    fn init_widgets(&self) {
        let self_ = imp::QRCodeRow::from_instance(self);
        let controller = gtk::GestureClick::new();
        controller.connect_pressed(glib::clone!(@weak self as row => move |_,_,_,_| {
            row.activate();
        }));

        self.add_controller(&controller);
        self_.picture.get().set_paintable(Some(&self_.paintable));
        self_.created_at_label.get().set_label(&format!(
            "Saved {}",
            self.code().created_at().format("%A %d, %B %G")
        ));

        self_.paintable.set_qrcode(self.code().data());

        let kind_widget = self.code().kind().widget();
        self_.container.get().prepend(&kind_widget);
    }

    fn init_actions(&self) {
        let self_ = imp::QRCodeRow::from_instance(self);

        action!(self_.actions, "copy", move |_, _| {});

        action!(
            self_.actions,
            "delete",
            glib::clone!(@weak self as row =>  move |_, _| {
                row.delete();
            })
        );
        self.insert_action_group("row", Some(&self_.actions));
    }
}
