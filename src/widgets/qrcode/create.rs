use super::QRCodePaintable;
use crate::qrcode::{QRCode, QRCodeData};
use anyhow::Result;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
use gtk::{gio, glib, CompositeTemplate};
use gtk_macros::action;

mod imp {
    use super::*;
    use glib::subclass;
    use once_cell::sync::OnceCell;

    #[derive(CompositeTemplate)]
    pub struct QRCodeCreatePage {
        pub actions: gio::SimpleActionGroup,
        #[template_child]
        pub picture: TemplateChild<gtk::Picture>,
        #[template_child]
        pub textview: TemplateChild<gtk::TextView>,
        pub paintable: QRCodePaintable,
        // WiFi
        pub wifi_encryption_algorithm: OnceCell<String>,
        #[template_child]
        pub password_entry: TemplateChild<gtk::PasswordEntry>,
        #[template_child]
        pub qr_type_stack: TemplateChild<gtk::Stack>,
        #[template_child]
        pub ssid_entry: TemplateChild<gtk::Entry>,
        #[template_child]
        pub ssid_visible_switch: TemplateChild<gtk::Switch>,
    }

    impl ObjectSubclass for QRCodeCreatePage {
        const NAME: &'static str = "QRCodeCreatePage";
        type Type = super::QRCodeCreatePage;
        type ParentType = gtk::Box;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.set_template_from_resource("/com/belmoussaoui/Decoder/ui/qrcode_create.ui");
            Self::bind_template_children(klass);
            klass.add_signal(
                "created",
                glib::SignalFlags::RUN_FIRST,
                &[QRCode::static_type()],
                glib::Type::Unit,
            );
        }

        fn instance_init(obj: &subclass::InitializingObject<Self::Type>) {
            obj.init_template();
        }

        fn new() -> Self {
            let wifi_encryption_algorithm = OnceCell::new();
            let _ = wifi_encryption_algorithm.set("WPA".to_string());
            Self {
                actions: gio::SimpleActionGroup::new(),
                picture: TemplateChild::default(),
                textview: TemplateChild::default(),
                paintable: QRCodePaintable::new(),
                wifi_encryption_algorithm,
                password_entry: TemplateChild::default(),
                qr_type_stack: TemplateChild::default(),
                ssid_entry: TemplateChild::default(),
                ssid_visible_switch: TemplateChild::default(),
            }
        }
    }

    impl ObjectImpl for QRCodeCreatePage {
        fn constructed(&self, obj: &Self::Type) {
            obj.init_widgets();
            obj.init_actions();
            self.parent_constructed(obj);
        }
    }

    impl WidgetImpl for QRCodeCreatePage {}
    impl BoxImpl for QRCodeCreatePage {}
}

glib::wrapper! {
    pub struct QRCodeCreatePage(ObjectSubclass<imp::QRCodeCreatePage>) @extends gtk::Widget, gtk::Box;
}

impl QRCodeCreatePage {
    pub fn new() -> Self {
        glib::Object::new(&[]).expect("Failed to create a QRCodeCreatePage widget")
    }

    fn init_widgets(&self) {
        let self_ = imp::QRCodeCreatePage::from_instance(self);
        self_.picture.get().set_paintable(Some(&self_.paintable));

        self_.textview.get().get_buffer().connect_changed(
            glib::clone!(@weak self as page => move |buffer| {
                page.regen_text_qrcode();
            }),
        );

        self_
            .qr_type_stack
            .get()
            .connect_property_visible_child_name_notify(
                glib::clone!(@weak self as page => move |stack| {
                    match stack.get_visible_child_name().unwrap().as_str() {
                        "text" => page.regen_text_qrcode(),
                        _ => page.regen_wifi_qrcode(),
                    };
                }),
            );

        self_.ssid_entry.get().connect_changed(
            glib::clone!(@weak self as page => move |_| {
                page.regen_wifi_qrcode();
            }),
        );

        self_.password_entry.get().connect_changed(
            glib::clone!(@weak self as page => move |_| {
                page.regen_wifi_qrcode();
            }),
        );

        self_.ssid_visible_switch.get().connect_state_set(
            glib::clone!(@weak self as page => move |_, _| {
                page.regen_wifi_qrcode();
                glib::signal::Inhibit(false)
            }),
        );
    }

    fn regen_text_qrcode(&self) {
        let self_ = imp::QRCodeCreatePage::from_instance(self);
        let buffer = self_.textview.get().get_buffer();
        let (start_iter, end_iter) = buffer.get_bounds();
        let content = buffer.get_text(&start_iter, &end_iter, true);
        self.regen_qrcode(&content);
    }

    fn regen_wifi_qrcode(&self) {
        let content = self.generate_wifi_qr();
        self.regen_qrcode(&content);
    }

    fn generate_text_qr(&self) -> String {
        let self_ = imp::QRCodeCreatePage::from_instance(self);
        let buffer = self_.textview.get().get_buffer();

        let (start_iter, end_iter) = buffer.get_bounds();
        buffer.get_text(&start_iter, &end_iter, true).into()
    }

    fn generate_wifi_qr(&self) -> String {
        // WiFi Codes are of the following form
        // WIFI:T:<ALGORITHM>;S:<SSID>;P:<PASSWORD>;H:<VISIBLE>;;
        let self_ = imp::QRCodeCreatePage::from_instance(self);

        let ssid = self_.ssid_entry.get().get_text().unwrap();
        let password = self_.password_entry.get().get_text().unwrap();
        let algorithm = self_.wifi_encryption_algorithm.get().unwrap();
        let hidden = self_.ssid_visible_switch.get().get_active().to_string();
        format!("WIFI:T:{};S:{};P:{};H:{};;", algorithm, ssid, password, hidden)
    }

    fn init_actions(&self) {
        let self_ = imp::QRCodeCreatePage::from_instance(self);

        action!(self_.actions, "copy", move |_, _| {});

        action!(
            self_.actions,
            "save",
            glib::clone!(@weak self as page => move |_, _| {
                page.save_code();
            })
        );

        action!(self_.actions, "set_none", glib::clone!(@weak self as page => move |_, _| {
            let _ = page.set_encryption_algorithm("nopass");
            page.regen_wifi_qrcode();
        }));

        action!(self_.actions, "set_wpa", glib::clone!(@weak self as page => move |_, _| {
            let _ = page.set_encryption_algorithm("WPA");
            page.regen_wifi_qrcode();
        }));

        action!(self_.actions, "set_wep", glib::clone!(@weak self as page => move |_, _| {
            let _ = page.set_encryption_algorithm("WEP");
            page.regen_wifi_qrcode();
        }));

        self.insert_action_group("create", Some(&self_.actions));
    }

    pub fn save_code(&self) -> Result<()> {
        let self_ = imp::QRCodeCreatePage::from_instance(self);

        let content = match self_.qr_type_stack.get().get_visible_child_name().unwrap().as_str() {
                        "text" => self.generate_text_qr(),
                        _ => self.generate_wifi_qr(),
        };

        let code = QRCode::create(&content)?;

        self.emit("created", &[&code])?;
        Ok(())
    }

    pub fn set_encryption_algorithm(&self, algorithm: &str) {
        let self_ = imp::QRCodeCreatePage::from_instance(self);

        let _ = self_.wifi_encryption_algorithm.set(algorithm.to_string());
    }

    pub fn regen_qrcode(&self, for_content: &str) {
        let self_ = imp::QRCodeCreatePage::from_instance(self);

        let qrcode = QRCodeData::from(for_content);
        self_.paintable.set_qrcode(qrcode);
    }
}
