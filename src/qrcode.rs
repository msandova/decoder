use anyhow::Result;
use gtk::glib;
use gtk::prelude::*;
use gtk::subclass::prelude::*;
pub use imp::QRCodeData;
pub use kind::QRCodeKind;

pub mod kind {
    use super::*;
    use std::collections::HashMap;
    use url::Url;

    fn parse_content(content: &str, schema: &str) -> Result<HashMap<String, String>> {
        let mut output: HashMap<String, String> = HashMap::new();

        let mut modified_schema: String = String::from(schema).split_whitespace().collect();
        let mut modified_content: String = String::from(content).split_whitespace().collect();

        let mut key_start_idx = modified_schema.find("${");
        let mut key_end_idx = modified_schema.find("}");

        while key_start_idx.is_some() && key_end_idx.is_some() {
            let (_, after_str) = modified_schema.split_at(key_start_idx.unwrap());
            let mut key = String::from(after_str.split_at(after_str.find("}").unwrap()).0);
            key.remove(0); // Remove $
            key.remove(0); // Remove {

            let mut val = String::from("");
            if let Some(char_after_key) = modified_schema.chars().nth(key_end_idx.unwrap() + 1) {
                let content = modified_content.split_at(key_start_idx.unwrap()).1;

                if let Some(char_end_idx) = content.find(char_after_key) {
                    val.push_str(&content.split_at(char_end_idx).0);
                    modified_content = content.split_at(char_end_idx).1.to_string();
                }
            } else {
                let content = modified_content.split_at(key_start_idx.unwrap()).1;
                val.push_str(&content.split_at(content.len()).0);
            }

            output.insert(key, val.trim().to_string());
            modified_schema = String::from(modified_schema.split_at(key_end_idx.unwrap() + 1).1);

            key_start_idx = modified_schema.find("${");
            key_end_idx = modified_schema.find("}");
        }
        Ok(output)
    }

    #[derive(Debug, Clone)]
    pub enum QRCodeKind {
        Url(Url),
        Telephone(String),
        OAuth {
            issuer: String,
            username: String,
            secret: String,
            uri: String,
        },
        Location {
            latitude: String,
            longitude: String,
        },
        SMSTo {
            phone: String,
            content: String,
        },
        MailTo {
            to: String,
            subject: Option<String>,
            body: Option<String>,
        },
        Contact {
            name: String,
            email: String,
        },
        Wifi {
            network: String,
            encryption: String,
            password: String,
        },
        Event {
            summary: String,
            start_at: String,
            end_at: String,
            description: Option<String>,
            location: Option<String>,
        },
        // The default one
        Other(String),
    }

    impl QRCodeKind {
        pub fn widget(self) -> gtk::Widget {
            use crate::widgets::qrcode::kind;
            match self {
                QRCodeKind::Url(url) => kind::QRCodeUrl::new(url).upcast(),
                QRCodeKind::Wifi {
                    network,
                    password,
                    encryption,
                } => kind::QRCodeWifi::new(network, password, encryption).upcast(),

                QRCodeKind::Location {
                    latitude,
                    longitude,
                } => kind::QRCodeLocation::new(latitude, longitude).upcast(),
                QRCodeKind::SMSTo { phone, content } => {
                    kind::QRCodeSMS::new(phone, content).upcast()
                }
                QRCodeKind::MailTo { to, subject, body } => {
                    kind::QRCodeMail::new(to, subject, body).upcast()
                }
                QRCodeKind::Event {
                    summary,
                    start_at,
                    end_at,
                    description,
                    location,
                } => kind::QRCodeEvent::new(summary, start_at, end_at, description, location)
                    .upcast(),
                _ => kind::QRCodeOther::new("some stuff".to_string()).upcast(),
            }
        }
    }

    impl From<&str> for QRCodeKind {
        fn from(content: &str) -> Self {
            let mut kind = None;
            if let Ok(uri) = Url::parse(content) {
                kind = match uri.scheme() {
                    "mailto" => {
                        let data = parse_content(content, "mailto:${email}").unwrap();
                        Some(Self::MailTo {
                            to: data.get("email").unwrap().to_string(),
                            subject: None,
                            body: None,
                        })
                    }
                    "matmsg" => {
                        let data = parse_content(
                            content,
                            "MATMSG:TO:${email};SUB:${subject};BODY:${body};;",
                        )
                        .unwrap();
                        Some(Self::MailTo {
                            to: data.get("email").unwrap().to_string(),
                            subject: data.get("subject").map(|s| s.clone()),
                            body: data.get("body").map(|s| s.clone()),
                        })
                    }
                    "wifi" => {
                        let data =
                            parse_content(content, "wifi:T:${encryption};S:${wifi};P:${key};;")
                                .unwrap();
                        Some(Self::Wifi {
                            network: data.get("wifi").unwrap().to_string(),
                            encryption: data.get("encryption").unwrap().to_string(),
                            password: data.get("key").unwrap().to_string(),
                        })
                    }
                    "geo" => {
                        let data = parse_content(content, "geo:${latitude},${longitude}").unwrap();
                        Some(Self::Location {
                            latitude: data.get("latitude").unwrap().to_string(),
                            longitude: data.get("longitude").unwrap().to_string(),
                        })
                    }
                    "tel" => {
                        let data = parse_content(content, "tel:${tel}").unwrap();
                        Some(Self::Telephone(data.get("tel").unwrap().to_string()))
                    }
                    "begin" => {
                        let data = parse_content(content, "BEGIN:VEVENT\nSUMMARY:${summary}\nDTSTART:${date_start}\nDTEND:${date_end}\nLOCATION:${location}\nDESCRIPTION:${description}\nEND:VEVENT\n").unwrap();
                        Some(Self::Event {
                            summary: data.get("summary").unwrap().to_string(),
                            start_at: data.get("date_start").unwrap().to_string(),
                            end_at: data.get("date_end").unwrap().to_string(),
                            location: data.get("location").map(|s| s.clone()),
                            description: data.get("description").map(|s| s.clone()),
                        })
                    }
                    "smsto" => {
                        let data = parse_content(content, "smsto:${tel}:${content}").unwrap();
                        Some(Self::SMSTo {
                            phone: data.get("tel").unwrap().to_string(),
                            content: data.get("content").unwrap().to_string(),
                        })
                    }
                    "mecard" => {
                        let data =
                            parse_content(content, "MECARD:N:${name};EMAIL:${email};").unwrap();
                        Some(Self::Contact {
                            name: data.get("name").unwrap().to_string(),
                            email: data.get("email").unwrap().to_string(),
                        })
                    }
                    "http" | "https" => Some(Self::Url(uri)),
                    _ => None,
                };
            }
            kind.unwrap_or_else(|| Self::Other(content.to_string()))
        }
    }
}

pub mod db {
    use crate::schema::codes;

    #[derive(Insertable, PartialEq, Debug)]
    #[table_name = "codes"]
    pub struct NewCode {
        pub content: String,
    }

    #[derive(Identifiable, Queryable, Hash, PartialEq, Eq, Debug, Clone)]
    #[table_name = "codes"]
    pub struct Code {
        pub id: i32,
        pub content: String,
        pub created_at: chrono::NaiveDateTime,
    }
}

mod imp {
    use super::*;
    use glib::subclass;
    use std::cell::{Cell, RefCell};

    #[derive(Debug, Clone, glib::GBoxed)]
    #[gboxed(type_name = "QRCodeData")]
    pub struct QRCodeData {
        pub width: i32,
        pub height: i32,
        pub items: Vec<Vec<bool>>,
    }

    impl From<&str> for QRCodeData {
        fn from(data: &str) -> Self {
            let code = qrcode::QrCode::new(data.as_bytes()).unwrap();
            let items = code
                .render::<char>()
                .quiet_zone(false)
                .module_dimensions(1, 1)
                .build()
                .split('\n')
                .into_iter()
                .map(|line| {
                    line.chars()
                        .into_iter()
                        .map(|c| !c.is_whitespace())
                        .collect::<Vec<bool>>()
                })
                .collect::<Vec<Vec<bool>>>();

            let width = items.get(0).unwrap().len() as i32;
            let height = items.len() as i32;
            Self {
                width,
                height,
                items,
            }
        }
    }
    static PROPERTIES: [subclass::Property; 2] = [
        subclass::Property("id", |name| {
            glib::ParamSpec::int(
                name,
                "id",
                "Id",
                0,
                i32::MAX,
                0,
                glib::ParamFlags::READWRITE | glib::ParamFlags::CONSTRUCT_ONLY,
            )
        }),
        subclass::Property("content", |name| {
            glib::ParamSpec::string(
                name,
                "content",
                "Content",
                None,
                glib::ParamFlags::READWRITE,
            )
        }),
    ];
    #[derive(Debug)]
    pub struct QRCode {
        pub id: Cell<i32>,
        pub content: RefCell<String>,
        pub data: RefCell<Option<QRCodeData>>,
        pub kind: RefCell<Option<QRCodeKind>>,
        pub created_at: RefCell<chrono::DateTime<chrono::Utc>>,
    }

    impl ObjectSubclass for QRCode {
        const NAME: &'static str = "QRCode";
        type ParentType = glib::Object;
        type Type = super::QRCode;
        type Instance = subclass::simple::InstanceStruct<Self>;
        type Class = subclass::simple::ClassStruct<Self>;

        glib::object_subclass!();

        fn class_init(klass: &mut Self::Class) {
            klass.install_properties(&PROPERTIES);
        }

        fn new() -> Self {
            Self {
                id: Cell::new(0),
                content: RefCell::new("".to_string()),
                data: RefCell::new(None),
                kind: RefCell::new(None),
                created_at: RefCell::new(chrono::Utc::now()),
            }
        }
    }

    impl ObjectImpl for QRCode {
        fn set_property(&self, _obj: &Self::Type, id: usize, value: &glib::Value) {
            let prop = &PROPERTIES[id];

            match *prop {
                subclass::Property("id", ..) => {
                    let id = value.get().unwrap().unwrap();
                    self.id.replace(id);
                }
                subclass::Property("content", ..) => {
                    let content = value.get().unwrap().unwrap();
                    self.content.replace(content);
                }
                _ => unimplemented!(),
            }
        }

        fn get_property(&self, _obj: &Self::Type, id: usize) -> glib::Value {
            let prop = &PROPERTIES[id];

            match *prop {
                subclass::Property("id", ..) => self.id.get().to_value(),
                subclass::Property("content", ..) => self.content.borrow().to_value(),
                _ => unimplemented!(),
            }
        }
    }
}

glib::wrapper! {
    pub struct QRCode(ObjectSubclass<imp::QRCode>);
}

impl QRCode {
    pub fn new(id: i32, content: &str, created_at: chrono::NaiveDateTime) -> Self {
        let qr_code: QRCode = glib::Object::new(&[("id", &id), ("content", &content)])
            .expect("Failed to create a QRCode object");
        let self_ = imp::QRCode::from_instance(&qr_code);
        let qrcode_data = imp::QRCodeData::from(content);
        self_.data.replace(Some(qrcode_data));
        let kind = QRCodeKind::from(content);
        self_.kind.replace(Some(kind));
        let time = chrono::DateTime::<chrono::Utc>::from_utc(created_at, chrono::Utc);
        self_.created_at.replace(time);
        qr_code
    }

    pub fn create(code_data: &str) -> Result<Self> {
        use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
        let values = db::NewCode {
            content: code_data.to_string(),
        };
        use crate::schema::codes;
        let conn = crate::database::connection().get()?;

        diesel::insert_into(codes::table)
            .values(values)
            .execute(&conn)?;

        codes::table
            .order(codes::columns::id.desc())
            .first::<db::Code>(&conn)
            .map_err(From::from)
            .map(From::from)
    }

    pub fn delete(&self) -> Result<()> {
        use diesel::{ExpressionMethods, QueryDsl, RunQueryDsl};
        let conn = crate::database::connection().get()?;
        use crate::schema::codes;

        diesel::delete(codes::table.filter(codes::columns::id.eq(&self.id()))).execute(&conn)?;
        Ok(())
    }

    pub fn load() -> Result<impl Iterator<Item = Self>> {
        use crate::schema::codes::dsl::*;
        use diesel::RunQueryDsl;
        let conn = crate::database::connection().get()?;

        Ok(codes.load::<db::Code>(&conn)?.into_iter().map(From::from))
    }

    pub fn id(&self) -> i32 {
        let self_ = imp::QRCode::from_instance(self);
        self_.id.get()
    }

    pub fn created_at(&self) -> chrono::DateTime<chrono::Utc> {
        let self_ = imp::QRCode::from_instance(self);
        self_.created_at.borrow().clone()
    }

    pub fn content(&self) -> String {
        let self_ = imp::QRCode::from_instance(self);
        self_.content.borrow().clone()
    }

    pub fn data(&self) -> QRCodeData {
        let self_ = imp::QRCode::from_instance(self);
        self_.data.borrow().as_ref().unwrap().clone()
    }

    pub fn kind(&self) -> QRCodeKind {
        let self_ = imp::QRCode::from_instance(self);
        self_.kind.borrow().as_ref().unwrap().clone()
    }
}

impl From<db::Code> for QRCode {
    fn from(code: db::Code) -> Self {
        Self::new(code.id, &code.content, code.created_at)
    }
}
