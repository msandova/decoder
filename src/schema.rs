table! {
    codes (id) {
        id -> Integer,
        content -> Text,
        created_at -> Timestamp,
    }
}
